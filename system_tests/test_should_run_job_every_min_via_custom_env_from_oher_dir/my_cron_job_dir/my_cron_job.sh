#!/bin/sh

# Testing cron job. Writes a new line into /my_cron_job/my_cron_job.log upon each invocation
echo "Cron job run via $1." >> /my_cron_job/my_cron_job.log

