#!/bin/sh

# Testing cron job. Writes a new line into /cron_job/cron_job.log upon each invocation
echo "Cron job run via $1." >> /cron_job/cron_job.log

