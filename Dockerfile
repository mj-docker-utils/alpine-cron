FROM alpine:latest

# CRON_TIMES specifies the desired timing in the standard cron format. Use tabulators to separate the values.
# Default is a schedule to run the cron task every hour
#
# Format:       min   hour  day   month weekday
ENV CRON_TIMES="0\t*\t*\t*\t*"

# CRON_RUN_ON_START specifies, whether the cron job should be run on startup of the container
ENV CRON_RUN_ON_START="0"

# The path to the script to be run by the cron
ENV CRON_JOB_PATH="/cron_job/cron_job.sh"

# Deploy the scripts directory with all the content and grant execution rights
COPY alpine_cron_scripts /alpine_cron_scripts
RUN chmod +x /alpine_cron_scripts/*.sh

# Initialize and run the cron daemon on the container startup
CMD /alpine_cron_scripts/initialize_and_run_cron.sh
