#!/bin/sh

# This script is registered into the /etc/crontab to be called from the cron daemon according to the schedule.
# It just calls the use defined cron job and passes the first argument, which is either "initialize", if run 
# from the CMD, or "cron" if run from the cron.

# run the cron job script
${CRON_JOB_PATH} $1
