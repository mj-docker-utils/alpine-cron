#!/bin/sh

# This script should be called only once during the container startup from the CMD clause.
# Generates the /etc/crontab to run the desired cron job and then runs the cron daemon

# Generate the cron schedule file. The only accepted file by the crond in alpine seems to be the /etc/crontabs/root
echo -e "${CRON_TIMES}\t/alpine_cron_scripts/cron_job_internal.sh \"cron\"\n" >> /etc/crontabs/root

# Run the cron job now, if requested
if [ "$CRON_RUN_ON_START" = "1" ]; then

    /alpine_cron_scripts/cron_job_internal.sh "initialize"
fi

# Run the cron daemon in the frontend
crond -f