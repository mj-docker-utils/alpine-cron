[![pipeline status](https://gitlab.com/mj-docker-utils/alpine-cron/badges/master/pipeline.svg)](https://gitlab.com/mj-docker-utils/alpine-cron/commits/master)

# Alpine-cron - Container with cron service based on the alpine Linux

Container capable of running scheduled job. Based on the alpine linux and easily enhanceable.

Image uses several environment variables, that are used for the schedule configuration:
 - CRON_TIMES - specifies the desired timing in the standard cron format. Use tabulators to separate the values (like in the crontab). Default is a schedule to run the cron task every hour.
 - CRON_RUN_ON_START - specifies, whether the cron job should be run on startup of the container.
 - CRON_JOB_PATH - path to the script that is run by the cron. By default the file /cron_job/cron_job.sh is used. You can override the behavior either by mapping a volume which contains the script file cron_job.sh into the directory /cron_job or by changing the environment variable to point to your custom cron job script file. The argument "initialize" respectivery "cron" is passed to the cron job script accordingly, if the job was run on the container startup respectively from the timer.

## Technical description

The container uses the standard cron daemon from the alpine Linux, configures it using /etc/crontabs/root file and runs it in the CMD command. To allow the modification of the container behavior during the run time by the ENV variable (see above), the initialization of the internal crontab is done as part of the CMD command in the script initialize_and_run_cron.sh. If this image is used as a base for creation derived images (using the FROM clause in the Dockerfile), the CMD command needs to still contain the run of /alpine_cron_scripts/initialize_and_run_cron.sh to make sure the crond is initialized and running.

## Usage

### Run instance of registry.gitlab.com/mj-docker-utils/alpine-cron:latest with your schedule

The simpliest usage is to use the latest version of the built image and specify the cron schedule parameters and map the cron job script
via the command line arguments.

    docker run                                                                    \
	  -v ./cron_job_dir:/cron_job_dir                                             \
	  -e "CRON_TIMES=*  *   *   *   *"                                            \
	  --name my_cron_contanier                                                    \
	  registry.gitlab.com/mj-docker-utils/alpine-cron:latest

Note: use the tabulators to separate the values in the CRON_TIMES argument.